package com.bootcamp;

import java.util.Scanner;

class Task4 {
    public static void main(String[] args) {
        Scanner angka = new Scanner(System.in);
        System.out.println("Enter Integer numbers: ");
        // String input
        int num1 = angka.nextInt();
        // Numerical input
        int num2 = angka.nextInt();

        // Output input by user
        System.out.println("Penjumlahan dua angka pertama yaitu: "+ (num1+num2));
    }
}
